from fastapi import FastAPI

app= FastAPI()

@app.get("/")
async def root():
    return {"message": "Hola Mundo"}

#Url Local: http://127.0.0.1:8000
#Detener el server: CTRL+C

@app.get("/url")
async def url():
    return {"url": "https://google.com.co"}

#Documentación con Swagger: http://127.0.0.1:8000/docs
#Documentación con Redocly: http://127.0.0.1:8000/redoc
