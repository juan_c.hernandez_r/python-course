from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

app= FastAPI()

# Inicia el server: uvicorn users:app --reload

class User(BaseModel):
    id: int
    name: str
    surname: str
    url: str 
    age: int 

users_list = [User(id=1, name="Camilo", surname="Hernandez", url="https://camilohreina.dev", age=35),
              User(id=2, name="Camilo", surname="Dev",
                   url="https://camilohreina.com", age=35),
              User(id=3, name="Camilo", surname="Dahlberg", url="https://camilohreina.com", age=33)]

@app.get("/usersjson")
async def usersjson():  # Creamos un JSON a mano
    return [{"name": "Camilo", "surname": "Moure", "url": "https://camilohreina.dev", "age": 35},
            {"name": "Camilo", "surname": "Dev",
                "url": "https://camilohreina.com", "age": 35},
            {"name": "Camilo", "surname": "Dahlberg", "url": "https://camilohreina.com", "age": 33}]


@app.get("/users")
async def users():
    return users_list
#Path
#http://127.0.0.1:8000/user/2
@app.get("/user/{id}")
async def user(id: int):
    return search_user(id)

#Query
#http://127.0.0.1:8000/userquery/?id=2
@app.get("/userquery/")
async def user(id: int):
    return search_user(id)

@app.post("/user/", response_model=User, status_code=201)
async def user(user: User):  
    if type(search_user(user.id)) == User:
        raise HTTPException(status_code=404, detail="El usuario ya existe")
    else: 
        users_list.append(user)
        return user

@app.put("/user/")
async def user(user: User):
    found=False
    for index, saved_user in enumerate(users_list):
        if saved_user.id == user.id:
            users_list[index]=user
            found= True
    if not found: 
        return {"error": "No se ha encontrado el usuario"}
    else:
        return user  

@app.delete("/user/{id}")
async def user(id: int):
    found=False
    for index, saved_user in enumerate(users_list):
        if saved_user.id == id:
            found= True
            del users_list[index]
    if not found: 
        return {"error": "No se ha eliminado el usuario"}

def search_user(id: int): 
    user = filter(lambda user: user.id == id, users_list)
    try:
        return list(user)[0]
    except:
        return {"error": "No se ha encontrado el usuario"}