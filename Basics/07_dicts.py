### Dictionaries ###

# Definición

my_dict = dict() #Definir un diccionario
my_other_dict = {}

print(type(my_dict))
print(type(my_other_dict))

my_other_dict = {"Nombre": "Camilo",
                 "Apellido": "Hernandez", "Edad": 35, 1: "Python"}

my_dict = {
    "Nombre": "Camilo",
    "Apellido": "Hernandez",
    "Edad": 35,
    "Lenguajes": {"Python", "Swift", "Kotlin"},
    1: 1.77
}

print(my_other_dict)
print(my_dict)

print(len(my_other_dict))
print(len(my_dict))

# Búsqueda

print(my_dict[1])
print(my_dict["Nombre"])

print("Hernandez" in my_dict)
print("Apellido" in my_dict)

# Inserción

my_dict["Calle"] = "Calle HernandezDev"
print(my_dict)

# Actualización

my_dict["Nombre"] = "Pedro"
print(my_dict["Nombre"])

# Eliminación

del my_dict["Calle"]
print(my_dict)

# Otras operaciones

print(my_dict.items()) #Retorna en pares clave,valor
print(my_dict.keys()) #Retorna las claves
print(my_dict.values()) #Retorna los valores

my_list = ["Nombre", 1, "Piso"]

my_new_dict = dict.fromkeys((my_list))  # Crear un dicionario sin valores, solo claves desde una lista
print(my_new_dict)
my_new_dict = dict.fromkeys(("Nombre", 1, "Piso"))  # Crear un dicionario sin valores, solo claves
print((my_new_dict))
my_new_dict = dict.fromkeys(my_dict) #Hace una copia del diccionario pero sin valores ej Nombre: None
print((my_new_dict))
my_new_dict = dict.fromkeys(my_dict, "HernandezDev") #Le pone a todas las claves el mismo valor
print((my_new_dict))

my_values = my_new_dict.values() #class -dict_values-
print(type(my_values))

print(my_new_dict.values())
print(list(dict.fromkeys(list(my_new_dict.values())).keys()))
print(tuple(my_new_dict))
print(set(my_new_dict))