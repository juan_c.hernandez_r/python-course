### Tuples ###
'''
Es un conjunto de valores inmutables, 
solo se pueden asignar en un principio 
'''

# Definición

my_tuple = tuple() #Definir una tupla
my_other_tuple = () #Definir una tupla

my_tuple = (35, 1.77, "Camilo", "Hernandez", "Camilo")
my_other_tuple = (35, 60, 30)

print(my_tuple)
print(type(my_tuple)) #type - tuple

# Acceso a elementos y búsqueda

print(my_tuple[0]) #35
print(my_tuple[-1]) #Camilo
# print(my_tuple[5]) IndexError
# print(my_tuple[-6]) IndexError

print(my_tuple.count("Camilo")) #2
print(my_tuple.index("Hernandez")) #Retorna el index donde aparece - 3 
print(my_tuple.index("Camilo"))

# my_tuple[1] = 1.80 'tuple' object does not support item assignment

# Concatenación

my_sum_tuple = my_tuple + my_other_tuple #Si permite concatenar tuplas
print(my_sum_tuple)

# Subtuplas

print(my_sum_tuple[3:6]) #Una subtubles que empieza en el indice 3

# Tupla mutable con conversión a lista

my_tuple = list(my_tuple)
print(type(my_tuple))

my_tuple[4] = "CamiloDev "
my_tuple.insert(1, "Azul")
my_tuple = tuple(my_tuple)
print(my_tuple)
print(type(my_tuple))

# Eliminación

# del my_tuple[2] TypeError: 'tuple' object doesn't support item deletion

del my_tuple #Si se puede borrar la tupla
# print(my_tuple) NameError: name 'my_tuple' is not defined