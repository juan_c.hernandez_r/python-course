# Variables: Se nombran todas en minusculas o snake case

my_string_variable = 'My String Var'
print(my_string_variable)

my_int_variable = 5
print(my_int_variable)

my_int_to_str_variable = str(my_int_variable)
print(my_int_to_str_variable)
print(type(my_int_to_str_variable))

my_bool_variable = True
print(my_bool_variable)

#Concatenación de variables en un print
print(my_string_variable, my_int_variable, my_bool_variable)
print(my_string_variable, my_int_to_str_variable, my_bool_variable)
print("Este es mi valor de :", my_bool_variable)

#Algunas Funciones del sistema
print(len(my_string_variable))

#Variables en una sola línea. ¡Cuidado con abusar de esta sintaxis!
name, surname, alias, age = "Camilo", "Hernandez", "Dev", 23
print("Me llamo:", name, surname, ". Mi edad es:", age, ". Y mi alias es:", alias)

# Inputs
name = input('¿Cuál es tu nombre? ')
age = input('¿Cuántos años tienes? ')
print(name)
print(age)

# Cambiamos su tipo
name = 35
age = "Brais"
print(name)
print(age)

# ¿Forzamos el tipo? En realidad no se fueraza nada
address: str = "Mi dirección"
address = True
address = 5
address = 1.2
print(type(address))