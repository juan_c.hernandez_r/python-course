### Strings ###

my_string = "Mi String"
my_other_string = 'Mi otro String'

print(len(my_string)) ##Hallar longitud del string
print(len(my_other_string))
print(my_string + " " + my_other_string)

my_new_line_string = "Este es un String\ncon salto de línea" ##Strings con formatos en este caso un salto de linea
print(my_new_line_string) 

my_tab_string = "\tEste es un String con tabulación" ## Formato de tabulación
print(my_tab_string)

my_scape_string = "\\tEste es un String \\n escapado" ## Escapa los strings
print(my_scape_string)

# Formateo

name, surname, age = "Camilo", "Hernandez", 23
print("Mi nombre es {} {} y mi edad es {}".format(name, surname, age)) #Con .format se ponen en corchetes en el orden que se pasen los strings
print("Mi nombre es %s %s y mi edad es %d" % (name, surname, age)) #Formateo de variables %s - strings %d - integers
print("Mi nombre es " + name + " " + surname + " y mi edad es " + str(age)) #No aconsejable
print(f"Mi nombre es {name} {surname} y mi edad es {age}") #Inferencia de datos - se pone la f delante

# Desempaqueado de caracteres

language = "python"
a, b, c, d, e, f = language #Se asigna una letra por cada variable
print(a) #Imprime - P
print(e) #Imprime - y

# División

language_slice = language[1:3] #Imprime de la posición 1 a la 3 sin contar la 3
print(language_slice) #yt

language_slice = language[1:] #Imprime de la posición 1 hasta el final
print(language_slice) #ython

language_slice = language[-2]
print(language_slice) #o

language_slice = language[0:6:2] #De 0 hasta 6 de 2 en dos
print(language_slice) #Pto

# Reverse

reversed_language = language[::-1] #
print(reversed_language) #nohtyP

# Funciones del lenguaje

print(language.capitalize()) #Primera letra en mayuscula - Python
print(language.upper()) # Toda en mayuscula - PYTHON
print(language.count("t")) # cuantas T tiene el string - 1
print(language.isnumeric()) #Si es un numero - False
print("1".isnumeric()) #Es un numero - True
print(language.lower()) # Toda minuscula
print(language.lower().isupper()) # Mayuscula y luego el isUpper valida si esta mayuscula - Retorna TRUE
print(language.startswith("Py")) #String inicia con
print("Py" == "py")  # No es lo mismo