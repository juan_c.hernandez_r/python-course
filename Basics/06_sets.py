### Sets ###


# Definición

my_set = set() #Definir un set - es un tipo de dato
my_other_set = {}

print(type(my_set))
print(type(my_other_set))  # Inicialmente es un diccionario

my_other_set = {"Camilo", "Hernandez", 35}
print(type(my_other_set)) # Aquí ya aparece como un -set-

print(len(my_other_set)) # 3

# Inserción

my_other_set.add("CamiloDev")

print(my_other_set)  # Un set no es una estructura ordenada

my_other_set.add("CamiloDev")  # Un set no admite repetidos

print(my_other_set)

# Búsqueda

print("Hernandez" in my_other_set) #True
print("Mouri" in my_other_set) #False

# Eliminación

my_other_set.remove("Hernandez")
print(my_other_set)

my_other_set.clear() #Set vacío
print(len(my_other_set))

del my_other_set
# print(my_other_set) NameError: name 'my_other_set' is not defined

# Transformación - muy arriesgado porque al no ser ordenado la lista puede quedar diferente en cada ejecución

my_set = {"Camilo", "Hernandez", 35}
my_list = list(my_set)
print(my_list)
print(my_list[0])

my_other_set = {"Kotlin", "Swift", "Python"}

# Otras operaciones

my_new_set = my_set.union(my_other_set) #Concatenar sets
print(my_new_set.union(my_new_set).union(my_set).union({"JavaScript", "C#"}))
print(my_new_set.difference(my_set)) #Retorna los diferentes en un set