### Lists ###

# Definición

my_list = list() #Se define una lista
my_other_list = [] #También se puede definir de esta manera

print(len(my_list)) #0 - Conjunto de datos, ahora esta vacío

my_list = [35, 24, 62, 52, 30, 30, 17]

print(my_list)
print(len(my_list)) # 7

my_other_list = [35, 1.77, "Camilo", "Hernandez"] #No siempre debe ser el mismo tipo de dato

print(type(my_list)) #class 'list'
print(type(my_other_list)) #class 'list'

# Acceso a elementos y búsqueda

print(my_other_list[0]) #Posición 0 - print 35
print(my_other_list[1]) #1.77
print(my_other_list[-1]) #Ultima posición
print(my_other_list[-4]) #35
print(my_list.count(30)) # 2 - Retorna la cantidad de veces que aparece un valor
# print(my_other_list[4]) IndexError
# print(my_other_list[-5]) IndexError

print(my_other_list.index("Camilo"))

age, height, name, surname = my_other_list #Se asignan los valores de la lista a variables
print(name) # Camilo

name, height, age, surname = my_other_list[2], my_other_list[1], my_other_list[0], my_other_list[3]
print(age) # 35

# Concatenación

print(my_list + my_other_list) #Concatenar listas
#print(my_list - my_other_list) #Error

# Creación, inserción, actualización y eliminación

my_other_list.append("CamiloDev") #Añade un elemento al final de la lista
print(my_other_list)

my_other_list.insert(1, "Rojo") #Lo inserta en el index y lo que se quiere insertar
print(my_other_list)

my_other_list[1] = "Azul" #Tambien se puede insertar por index de esta manera
print(my_other_list)

my_other_list.remove("Azul") #Para elminar se pasa el valor de lo que se quiere eliminar
print(my_other_list)

my_list.remove(30) #Si hay varios similares, solo elimina el primero que encuentre
print(my_list)

print(my_list.pop()) #Elimina el ultimo item de la lista
print(my_list)

my_pop_element = my_list.pop(2) #Remueve el elemento por index y lo guarda en una variable
print(my_pop_element)
print(my_list)

del my_list[2] #Elimina por indice
print(my_list)

# Operaciones con listas

my_new_list = my_list.copy() #Realiza una copia de la lista y la pone en una nueva variable

my_list.clear() #Borra la lista
print(my_list)
print(my_new_list)

my_new_list.reverse() #Da reversa a la lista
print(my_new_list)

my_new_list.sort() #Ordena la lista - Por default de menor a mayor
print(my_new_list)

# Sublistas

print(my_new_list[1:3]) #Hacer una lista nueva desde un indice a otro

# Cambio de tipo

my_list = "Hola Python"
print(my_list) #Tiene cambio de tipado dinamico
print(type(my_list))