### Operadores Aritméticos ###

# Operaciones con enteros
print(3 + 4)
print(3 - 4)
print(3 * 4)
print(3 / 4)
print(10 % 3) # Operador de modulo - Resto de una división
print(10 // 3) # Floor división - Intenta aproximar a un valor entero - 3
print(2 ** 3) # Calculo de exponencial - 8
print(2 ** 3 + 3 - 7 / 1 // 4)

# Operaciones con cadenas de texto
print("Hola " + "Python " + "¿Qué tal?") #Concatenar strings
print("Hola " + str(5)) #Si se va concatenar con + deben ser todos strings

# Operaciones mixtas
print("Hola " * 5) #La palabra se multiploca y se muestra repetidamente 5 veces
print("Hola " * (2 ** 3))

my_float = 2.5 * 2
print("Hola " * int(my_float))

### Operadores Comparativos ###

# Operaciones con enteros
print(3 > 4)
print(3 < 4)
print(3 >= 4)
print(4 <= 4)
print(3 == 4)
print(3 != 4)

# Operaciones con cadenas de texto
print("Hola" > "Python")
print("Hola" < "Python")
print("aaaa" >= "abaa")  # Ordenación alfabética por ASCII
print(len("aaaa") >= len("abaa"))  # Cuenta caracteres
print("Hola" <= "Python")
print("Hola" == "Hola")
print("Hola" != "Python")

### Operadores Lógicos ###

# Basada en el Álgebra de Boole https://es.wikipedia.org/wiki/%C3%81lgebra_de_Boole
print(3 > 4 and "Hola" > "Python") #False
print(3 > 4 or "Hola" > "Python") #True
print(3 < 4 and "Hola" < "Python") #True
print(3 < 4 or "Hola" > "Python") #True
print(3 < 4 or ("Hola" > "Python" and 4 == 4)) #True
print(not (3 > 4)) #True